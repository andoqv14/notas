package notasalumnos;

import java.util.ArrayList;
import java.util.Scanner;

public class NotasAlumnos {

    public static void Inicializar(double calificaciones[], String alumnos[]
            , Scanner teclado) {
        for (int i = 0; i < calificaciones.length; i++) {
            System.out.println("Nombre del Alumno: ");
            alumnos[i] = teclado.next();
            System.out.println("Calificacion: ");
            calificaciones[i] = teclado.nextDouble();
        }
    }

    public static double Promedio(double calificaciones[]) {
        double prom, suma = 0;
        for (int i = 0; i < calificaciones.length; i++) {
            suma += calificaciones[i];
        }
        prom = suma / calificaciones.length;
        return prom;
    }

    public static void Imprimir(double calificaciones[], String Alumnos[]
            , double Promedio) {
        System.out.printf("%-15s %-15s%n%n", "Calificacion");
        for (int i = 0; i < calificaciones.length; i++) {
            System.out.printf("%-15s %-15f%n", Alumnos[i], calificaciones[i]);
        }
        System.out.println("El promedio grupal es "+ Promedio);
    }
    
    public static void Mayor(double calificaciones[], String alumnos[]) {
        double Mayor =0;
        String Alumno = null;
        for (int i = 0; i < calificaciones.length; i++) {
            if (calificaciones[i] > Mayor) {
                Mayor = calificaciones[i];
                Alumno = alumnos[i];
            }
        }
        System.out.println("\nLa Calificacion Mas Alta fue: " + Mayor
                + " y la saco " + Alumno);
    }

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        double calificaciones[];
        String alumnos[];
        int n;
        double Promedio;

        System.out.println("Ingrese la cantidad de alumnos");
        n = teclado.nextInt();

        calificaciones = new double[n];
        alumnos = new String[n];

        Inicializar(calificaciones, alumnos, teclado);
        Promedio = Promedio(calificaciones);
        Imprimir(calificaciones, alumnos, Promedio);
        Mayor(calificaciones, alumnos);
    }

}
